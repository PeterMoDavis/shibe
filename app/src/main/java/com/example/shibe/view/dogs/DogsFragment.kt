package com.example.shibe.view.dogs

import android.graphics.Color
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.example.shibe.adapter.ShibeAdapter
import com.example.shibe.databinding.FragmentDogsBinding
import com.example.shibe.model.ShibeRepo

import com.example.shibe.viewmodel.ShibeViewModel
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class DogsFragment() : Fragment() {
    private var _binding: FragmentDogsBinding? = null
    private val binding get() = _binding!!
    @Inject lateinit var repo : ShibeRepo
    private val shibeViewModel by viewModels<ShibeViewModel>()



    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = FragmentDogsBinding.inflate(inflater, container, false).also {
        _binding = it
    }.root

    var liked = false

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        shibeViewModel.state.observe(viewLifecycleOwner) { state ->
            binding.rvDogList.apply {
                adapter = ShibeAdapter(state.shibes, repo)
                with(binding){
                    btnGrid.setOnClickListener() {
                        layoutManager = GridLayoutManager(context, 3)
                    }
                   btnLinear.setOnClickListener() {
                        layoutManager = LinearLayoutManager(context)
                    }
                    btnStaggered.setOnClickListener() {
                        layoutManager = StaggeredGridLayoutManager(2, 1)
                    }

                }

            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }

}


