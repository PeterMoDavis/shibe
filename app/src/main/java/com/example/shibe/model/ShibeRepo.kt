package com.example.shibe.model

import android.content.Context
import android.util.Log
import com.example.shibe.model.local.ShibeDatabase
import com.example.shibe.model.local.entity.Shibe
import com.example.shibe.model.remote.ShibeService
import dagger.hilt.android.qualifiers.ApplicationContext
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import javax.inject.Inject
import javax.inject.Singleton


private const val TAG = "ShibeRepo"

@Singleton
class ShibeRepo @Inject constructor(private val shibeService: ShibeService, @ApplicationContext context: Context) {
    val shibeDao = ShibeDatabase.getInstance(context).shibeDao()

    suspend fun getShibes() = withContext(Dispatchers.IO) {
        val cachedShibes: List<Shibe> = shibeDao.getAll()

        return@withContext cachedShibes.ifEmpty() {
            val shibeUrls: List<String> = shibeService.getShibes()
            Log.d(TAG, "shibeUrls size is ${shibeUrls.size}")
            val shibes: List<Shibe> = shibeUrls.map { Shibe(url = it) }
            Log.d(TAG, "shibes size is ${shibes.size}")
            shibeDao.insert(shibes)
            Log.d(TAG, "shibes size is ${shibeDao.getAll().size}")
            return@ifEmpty shibes
        }
    }
}