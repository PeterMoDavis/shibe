package com.example.shibe.model.local.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Update
import com.example.shibe.model.local.entity.Shibe

@Dao
interface ShibeDao {

    @Query("SELECT * FROM shibe_table")
    suspend fun getAll(): List<Shibe>

    @Insert
    suspend fun insert(shibe: List<Shibe>)

    @Update
    suspend fun update(shibe: Shibe)
}